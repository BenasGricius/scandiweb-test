<?php

namespace app;
$config = require 'config.php';
require 'Database.php';
$db = new Database($config['database']);


if(isset($_POST['add_product'])) {
    $data = [
        'sku'=> $_POST['sku'],
        'name'=> $_POST['name'],
        'price'=> $_POST['price'],
        'type_id'=> $_POST['type_id'],
        'size'=> $_POST['size'],
        'height'=> $_POST['height'],
        'width'=> $_POST['width'],
        'length'=> $_POST['length'],
        'weight'=> $_POST['weight'],
    ];    
    $db->query("INSERT INTO products (sku, name, price, type_id, size, height, width, length, weight) VALUES (:sku, :name, :price, :type_id, :size, :height, :width, :length, :weight)", $data);    
}

if(isset($_POST['mass_delete'])) {
    $ids = $_POST['selectedProductsIds'];
    $placeholders = implode(',', array_fill(0, count($ids), '?'));     
    $db->query("DELETE FROM products WHERE id IN ($placeholders)", $ids);
}
