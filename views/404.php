<?php
    require 'partials/header.php';
?>
<section class="products-section">
    <div class="container">
        <div class="intro-wrapper">
            <div class="title">
                <h1>
                    Page not found
                </h1>                    
            </div>
            <div class="btns-wrapper">
                <a href="/scandiweb" class="primary-button primary-button__add">
                    Back to Homepage
                </a>                
            </div>
        </div>      
    </div>        
</section>

<?php
    require 'partials/footer.php';
?>