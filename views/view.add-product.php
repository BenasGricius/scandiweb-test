<?php
    require 'partials/header.php';
?>
<section class="products-section">
    <div class="container">

        <div class="intro-wrapper">
            <div class="title">
                <h1>
                    Product add
                </h1>                    
            </div>
            <div class="btns-wrapper">
                <button class="save primary-button primary-button__add">
                    Save
                </button>                 
                <a href="/" class="primary-button primary-button__delete">
                    CANCEL
                </a>
            </div>
        </div>
        <form method="post" id="product_form">
            <div class="form-element-wrapper">
                <div class="form-element">
                    <label for="sku">SKU</label>
                    <input class="active" type="text" id="sku" name='sku' placeholder="Please enter SKU">
                    <span class="form-element-error-msg">
                        Please, submit required data
                    </span>
                    <span class="form-element-error-msg-type">
                        Please, provide the data of indicated type
                    </span>
                </div>
                <div class="form-element">
                    <label for="name">Name</label>
                    <input class="active" type="text" id="name"  name='name' placeholder="Please enter name">
                    <span class="form-element-error-msg">
                        Please, submit required data
                    </span>
                    <span class="form-element-error-msg-type">
                        Please, provide the data of indicated type
                    </span>
                </div>
            </div>
            <div class="form-element-wrapper">
                <div class="form-element">
                    <label for="price">Price ($)</label>
                    <input class="active" type="number" id="price" name='price' placeholder="Please enter price">
                    <span class="form-element-error-msg">
                        Please, submit required data
                    </span>
                    <span class="form-element-error-msg-type">
                        Please, provide the data of indicated type
                    </span>
                </div>
            </div>
           
            <div class="form-element-wrapper">
                <div class="form-element">
                    <label for="productType">Type</label>
                    <select name="productType" id="productType">
                        <option value="" selected disabled hidden>Choose here</option>
                        <?php $i = 0; ?>
                        <?php foreach ($types as $type):?>
                        <option value="<?php echo $type['id'];?>">
                            <?php echo $type['name'];?>
                        </option>
                        <?php endforeach; ?>
                    </select>
                    <span class="form-element-error-msg">
                        Please, submit required data
                    </span>
                    <span class="form-element-error-msg-type">
                        Please, provide the data of indicated type
                    </span>
                </div>
            </div>

            <div class="type-wrapper" id="1">
                <div class="form-element">
                    <label for="size">Size (MB)</label>
                    <input type="number" id="size" name='size' placeholder="Please enter size">
                    <span class="form-element-error-msg">
                        Please, submit required data
                    </span>
                    <span class="form-element-error-msg-type">
                        Please, provide the data of indicated type
                    </span>
                </div>
                
                <div class="product-desccription">
                    Please, provide size.
                </div>                    
            </div>
            
            <div class="type-wrapper" id="3">
                <div class="form-element">
                    <label for="height">Height (CM)</label>
                    <input type="number" id="height" name="height" placeholder="Please enter height">
                    <span class="form-element-error-msg">
                        Please, submit required data
                    </span>
                    <span class="form-element-error-msg-type">
                        Please, provide the data of indicated type
                    </span>
                </div>
                <div class="form-element">
                    <label for="width">Width (CM)</label>
                    <input type="number" id="width" name='width' placeholder="Please enter width">
                    <span class="form-element-error-msg">
                        Please, submit required data
                    </span>
                    <span class="form-element-error-msg-type">
                        Please, provide the data of indicated type
                    </span>
                </div>
                <div class="form-element">
                    <label for="length">Length (CM)</label>
                    <input type="number" id="length" name='length' placeholder="Please enter length">
                    <span class="form-element-error-msg">
                        Please, submit required data
                    </span>
                    <span class="form-element-error-msg-type">
                        Please, provide the data of indicated type
                    </span>
                </div>                
                <div class="product-desccription">
                    Please, provide dimentions in HxWxL format.
                </div>                    
            </div>
            <div class="type-wrapper" id="2">
                <div class="form-element">
                    <label for="Weight">Weight (KG)</label>
                    <input type="number" id="weight" name="weight" placeholder="Please enter weight">
                    <span class="form-element-error-msg">
                        Please, submit required data
                    </span>
                    <span class="form-element-error-msg-type">
                        Please, provide the data of indicated type
                    </span>
                </div>                               
                <div class="product-desccription">
                    Please, provide weight.
                </div>                    
            </div>
        </form>
        
        <div class="end-section">
            <div class="title">
                <h4>
                    Scandiweb Test assignment
                </h4>                    
            </div>            
        </div>              
    </div>        
</section>

<?php
    require 'partials/footer.php';
?>