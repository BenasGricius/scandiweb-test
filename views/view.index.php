<?php
    require 'partials/header.php'; 
?>
<section class="products-section">
    <div class="container">
        <div class="intro-wrapper">
            <div class="title">
                <h1>
                    Product list
                </h1>                    
            </div>
            <div class="btns-wrapper">
                <a href='/addproduct' class="primary-button primary-button__add">
                    ADD
                </a>
                <button class="primary-button primary-button__delete delete-js">
                    MASS DELETE
                </button>
            </div>
        </div>
        <div class="product-list">
            <ul>
                <?php foreach ($products as $product):?>
                <li>
                    
                    <input class='delete-checkbox' type="checkbox" id='<?php echo $product['id']; ?>'>
                    
                    <div class="product-id"><?php echo $product['sku']; ?></div>
                    <div class="product-name"><?php echo $product['name']; ?></div>
                    <div class="product-price"><?php echo $product['price'];?> $</div>
                    <?php if($product['size']): ?>
                        <div class="product-property">Size: <?php echo $product['size'];?> MB</div>
                    <?php endif; ?>
                    <?php if($product['weight']): ?>
                        <div class="product-property">Weight: <?php echo $product['weight'];?> KG</div>
                    <?php endif; ?>
                    <?php if($product['height'] && $product['width'] && $product['length']): ?>                        
                        <div class="product-property">Dimentions: <?php echo $product['height'];?>x<?php echo $product['width'];?>x<?php echo $product['length'];?></div>
                    <?php endif; ?>                                                                    
                </li>
                <?php endforeach; ?>                
            </ul>
        </div>
        <div class="end-section">
            <div class="title">
                <h4>
                    Scandiweb Test assignment
                </h4>                    
            </div>            
        </div>              
    </div>        
</section>

<?php
    require 'partials/footer.php';
?>