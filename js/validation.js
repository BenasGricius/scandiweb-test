
const formValidation = function (fields,select) {

    let valid = true;
    for (let i = 0; i < fields.length; i++) {
        fields[i].parentElement.classList.remove('form-element-error');
        fields[i].parentElement.classList.remove('form-element-error-msg-type');       

        if (fields[i].value === '') {
            fields[i].parentElement.classList.add('form-element-error');
            fields[i].parentElement.classList.remove('form-element-error-msg-type');
            valid = false;
        }
        else if (fields[i].value !== '' && fields[i].type === 'number') {
            fields[i].parentElement.classList.remove('form-element-error');
            var paternNumber =/^[\d ]+$/;
            if(!paternNumber.test(fields[i].value)){
                fields[i].parentElement.classList.add('form-element-error', 'form-element-error-msg-type');
                valid = false;
            }else{
                fields[i].parentElement.classList.remove('form-element-error', 'form-element-error-msg-type');
                fields[i].parentElement.classList.add('form-element-valid');
            }
        } 
        else {
            fields[i].parentElement.classList.remove('form-element-error');
            fields[i].parentElement.classList.add('form-element-valid');
        }
    }
    
    select.parentElement.classList.remove('form-element-error');
    select.parentElement.classList.remove('form-element-error-msg-type');
    if (select.value == ''){
        select.parentElement.classList.add('form-element-error');
        select.parentElement.classList.remove('form-element-error-msg-type');
        valid = false;
    }

    return valid;
}



export {formValidation}