<?php
namespace app;
$config = require 'config.php';
$db = new Database($config['database']);
$products = $db->query("select * from products")->fetchAll(\PDO::FETCH_ASSOC);
require 'views/view.index.php';
