<?php

$routes = require 'routes.php';

$uri = parse_url($_SERVER['REQUEST_URI'])['path'];

function routeToPage($uri, $routes) {

    if (array_key_exists($uri,$routes)) {
        require $routes[$uri];        
    }else{
        abort();       
    }
}

routeToPage($uri, $routes);
// dd($uri);

function abort($code = 404) {
    http_response_code($code);
    require "views/{$code}.php";
    die();
}


