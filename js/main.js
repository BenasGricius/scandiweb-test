
import {formValidation} from './validation.js';

if(document.getElementById('productType')) {

    let selectType =  document.getElementById('productType');
    let types = document.querySelectorAll('.type-wrapper');
    selectType.addEventListener('change',function(){
        let curr = this.value;
        for(var i=0; i<types.length; i++){
            types[i].classList.remove('show');
            types[i].querySelector('input').classList.remove('active');
            if(types[i].id === curr ){
                types[i].classList.add('show');                
                types[i].querySelector('input').classList.add('active');
            }
        }       
        
    })
}

if (document.querySelector('.save')){
    let save = document.querySelector('.save');    
    save.addEventListener('click', function(){
        let fields = document.querySelectorAll('#product_form input.active');
        let select = document.getElementById('productType');
        if(formValidation(fields,select)){            
            var sku = document.getElementById('sku').value;
            var name = document.getElementById('name').value;
            var price = document.getElementById('price').value;
            var type_id = document.getElementById('productType').value;
            var size = document.getElementById('size').value;
            var weight = document.getElementById('weight').value;
            var height = document.getElementById('height').value;
            var width = document.getElementById('width').value;
            var length = document.getElementById('length').value;           
            
            $.ajax({
                type: "POST",
                url: "ajax.php",
                data: { add_product: "add_product",sku,name,price,type_id,size,weight,height,width,length },
                success: function(response) {
                    console.log(response);
                    window.location.href='/';
                }
            });            
        }               
          
    });
}


if (document.querySelector('.delete-js')){
    let massDelete = document.querySelector('.delete-js');    
    massDelete.addEventListener('click', function(){
        let selectedProducts = document.querySelectorAll('.product-list input[type=checkbox]:checked');
        let selectedProductsIds = []
        for(var i = 0; i < selectedProducts.length; i++){
            selectedProductsIds.push(selectedProducts[i].id);            
        }
        
        if (selectedProductsIds.length){
            console.log(selectedProductsIds)       
            $.ajax({
                type: "POST",
                url: "ajax.php",
                data: { mass_delete: "mass_delete",selectedProductsIds },
                success: function(response) {
                    console.log(response);
                    window.location.href='/';
                }
            });
            
        }               
          
    });
}